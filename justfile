# list recipes
default:
  just --list --unsorted

# make pdf
pdf:
  cat src/toc.txt | xargs cat | pdfroff -i -ms -t -s -p > dist/wellness.pdf \
  && exiftool -overwrite_original_in_place -Author="43beans" -Title="welless" dist/wellness.pdf
  
# make txt
txt:
  cat src/toc.txt \
  | xargs cat \
  | nroff -ms -t -s -Tascii \
  | sed 's,\x1B\[[0-9;]*[a-zA-Z],,g' \
  > dist/wellness.txt
