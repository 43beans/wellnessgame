.so src/wellness.tmac
.ds TITLE wellness lite: a game
.
.
.\" Document Info
.TL
\*[TITLE]
.AU
\*[AUTHOR]
.XS
wellness lite
.XE
.
.
.
.\" Begin Document
.LP
You will need two distinguishable coins.
One is your BODY coin,
and the other is your MIND coin.
Flip them both.
.\" ::: rec start
.\" Body: H
.\" Mind: T
.\" Result: Do a physical activity
.\" 
.\" Body: T
.\" Mind: H
.\" Result: Do a mental activity
.\" 
.\" Body: H
.\" Mind: H
.\" Result: Do both
.\" 
.\" Body: T
.\" Mind: T
.\" Result: Say something nice about yourself and flip again
.\" ::: rec end
.\" ::: tbl start
.DS
.TS
tab(,);
lb lb lbx
-- -- --
l  l  lx.
Body,Mind,Result
H,T,Do a physical activity
T,H,Do a mental activity
H,H,Do both
T,T,T{
Say something nice about yourself and flip again
T}
.TE
.DE
.\" ::: tbl end
.\" ::: markdown start
.\" .DS
.\" .nf
.\" .ds FAM C
.\" \f[C]Body   Mind   Result\f[]
.\" ------ ------ --------------------------------------------------
.\" H      T      Do a physical activity
.\" T      H      Do a mental activity
.\" H      H      Do both
.\" T      T      Say something nice about yourself and flip again
.\" .fi
.\" .DE
.\" ::: markdown end
.2C
.LP
Example physical activities:
.IP \(rh
Stand up and reach for the sky, do some side bends.
.IP \(rh
Go for a short walk.
.IP \(rh
Drink some water.
.IP \(rh
Roll your wrists and massage your hands.
.IP \(rh
Touch your toes and dangle. sway side to side.
.IP \(rh
Choose your own.
.br
.ne 999
.LP
Example mental activities:
.IP \(rh
What are you grateful for?
.IP \(rh
Imagine your favorite place.
.IP \(rh
Take 12 mindful breaths.
.IP \(rh
Imagine happiness, health, and peace.
.IP \(rh
Say hi to a friend.
.IP \(rh
Choose your own.
.if n .pl \n[nl]u
.TC
