.TL
wellness: the rpg
.AU
Case Duckworth
.AI
.I " 43beans " joint a
.SH wellness: the rpg
.PP
.B wellness
is a lil rpg (my first!) that helps you stay ok when working.
it's pretty simple i think... we'll see.
.SH
Rules
.PP
For
.B wellnessrpg
you'll need 2 six-sided dice (d6).
Roll them both, then check the table below for what you should do
based on what they're divisible by.
If your roll is divisible by more than one, you can choose:
.IP (a)
Choose the higher factor.
.IP (b)
Choose which factor appears most
in the prime factorization of your dice roll.
.IP (c)
Pick whichever is your favorite!
.IP (d)
Do all of the ones that can factor into your dice roll.
.PP
Repeat this game until you feel better.
I recommend setting a timer or rolling 2d6 whenever you complete
a Work Task.
.SH
Stats
.PP
This game has a few stats that can modify gameplay:
.IP STRESS_MULTIPLIER
How stressed you are right now.
.TS
allbox ;
L L.
Not stressed / normal work stress	1
A bit piqued	2
Freaking out	3
More ? Oh no	Your choice -- and good luck <3
.TE
.rs
.sp 3
.ns
.SH
Dice roll table
.TS
allbox center ;
Cb Cb
Nf(R) Lf(R).
Dice roll	What to do
.\"  1  2  3  4  5  6  5  4  3  2  1
.\" 02 03 04 05 06 07 08 09 10 11 12
div2	Drink water		\" 18
div3	Get up and stretch	\" 12
div5	Box breathe x 4		\" 7
div7	Take a lap		\" 6
div11	Sit, close eyes, chill for d6*STRESS_MULTIPLER minutes	\" 2
.TE
