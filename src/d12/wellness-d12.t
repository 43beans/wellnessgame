.so src/wellness.tmac
.ds TITLE wellness d12: a game
.
.
.\" Document Info
.TL
\*[TITLE]
.AU
\*[AUTHOR]
.XS
wellness d12
.XE
.
.
.
.\" Begin Document
.heading How To Play
.LP
You will need one twelve sided dice.
Whenever you think about the game,
or whenever you notice yourself getting stressed,
or every thirty minutes,
roll your twelve sided dice.
Look up the result on the Wellness Table.
.IP NOTE:
Some numbers are present in more than one category.
This means you get to choose the result you want.
(Or the result you don't want, but need.)
.heading Wellness Table
.\" ::: tbl start
.DS
.TS
tab(;);
lb lb lbx
-- -- --
l  l  lx.
d12;Category;Result
2, 3, 5, 7, 11;prime;breath and meditation
3, 6, 9, 12;unit of 3;physical activity
4, 8, 12;unit of 4;set and setting
5, 10;unit of 5;massage and self soothing
1;1;ONE
.TE
.DE
.\" ::: tbl end
.\" ::: markdown start
.\" .DS
.\" .nf
.\"  \f[C]BEGINTABLE
.\"  d12              Category    Result
.\"  ---------------- ----------- ---------------------------
.\"  2, 3, 5, 7, 11   prime       breath and meditation
.\"  3, 6, 9, 12      unit of 3   physical activity
.\"  4, 8, 12         unit of 4   set and setting
.\"  5, 10            unit of 5   massage and self soothing
.\"  1                1           ONE
.\"  ENDTABLE
.\" .fi
.\" .DE
.\" ::: markdown end
.heading Example Activities
.IP "Breath and Meditation"
box breathing; interupted inhale breathing; slow count to ten; finger tracing breath; peaceful visualization; gratitude; mindfulness; body scan
.IP "Physical Activity"
short walk; standing stretch; balance exercise; drink water; seated stretch
.IP "Set and Setting"
tidy up your space; listen to a favorite song; go get some sunshine; look at something far away; say hi to a friend
.IP "Massage and Self Soothing"
take a short nap; have a tasty treat; massage your hands and roll out your wrists; neck rolls clockwise and anticlockwise; massage your temples and wrists.
.IP "ONE"
a powerful result. take the rest of the day off, or roll again.
.if n .pl \n[nl]u  \" if nroff, set page length to number of lines
.                  \" (prevent empty lines at end of text)
.bp
