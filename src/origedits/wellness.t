.\" a heading macro
.de heading
.sp 1
.LP
\f[B]\s+3\(rA \\$* \(lA\s0\f[]  \" Bolds, and embiggens
.sp 0.2
..
.nr table 0 1     \" a number register for keeping track
.                 \" of tables
.if n .pl 999i    \" set page length way long if nroff
.
.
.\" Document Info
.TL
wellness: the rpg
.AU
Case Duckworth
.AI
a \f[I]43beans\f[] joint
.
.
.
.\" Begin Document
.heading wellness: the rpg
.LP
.B wellness
is a lil rpg (my first!) that helps you stay ok when working.
it's pretty simple i think... we'll see.
.
.
.
.heading Rules
.LP
For
.B wellnessrpg
you'll need 2 six-sided dice (d6).
Roll them both, then check the table below for what you should do
based on what they're divisible by.
If your roll is divisible by more than one, you can choose:
.IP (a)
Choose the higher factor.
.IP (b)
Choose which factor appears most
in the prime factorization of your dice roll.
.IP (c)
Pick whichever is your favorite!
.IP (d)
Do all of the ones that can factor into your dice roll.
.LP
Repeat this game until you feel better.
I recommend setting a timer or rolling 2d6 whenever you complete
a Work Task.
.
.
.
.heading Stats
.LP
This game has a few stats that can modify gameplay:
.IP STRESS_MULTIPLIER
How stressed you are right now.
.LP
.RS
.TS
tab(;) ;
LB LB
-- --
Lx  L.
Stress Level;Multiplier
Not stressed / normal work stress;1
A bit piqued;2
Freaking out;3
More? Oh no;Your choice \(em and good luck <3
.T&
cb s.
T{
.sp 0.2
\s-1TABLE \n+[table]: STRESS_MULTIPLIER\s0
T}
.TE
.RE
.rs
.sp 2
.ns
.
.
.
.heading Dice roll table
.RS
.TS
linesize(6) tab(;);
Cb Cb Lb
-- -- --
Nf(R) Nf(R) Lf(R)x.
Divisor;Dice roll(s);What to do
.\"  1  2  3  4  5  6  5  4  3  2  1
.\" 02 03 04 05 06 07 08 09 10 11 12
2;2, 4, 6, 8, 10, 12;Drink water
3;3, 6, 9, 12;T{
Get up and stretch for d6*STRESS_MULTIPLER minutes
T}
5;5, 10;T{
Box breathe x 4*STRESS_MULTIPLER times
T}
7;7;Take a lap x STRESS_MULTIPLER
11;11;T{
Sit, close eyes, chill for d6*STRESS_MULTIPLER minutes
T}
.T&
cb s s.
T{
.sp 0.2
\s-1TABLE \n+[table]: Dice roll divisors and meanings\s0
T}
.TE
.RE
.if n .pl \n[nl]u
