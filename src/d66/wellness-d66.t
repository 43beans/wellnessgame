.so src/wellness.tmac
.ds TITLE wellness d66: a game
.
.
.\" Document Info
.TL
\*[TITLE]
.AU
\*[AUTHOR]
.XS
wellness d66
.XE
.
.
.
.\" Begin Document
.if t .2C
.heading How To Play
.LP
You will need:
.IP \(rh
1 - 2  six-sided dice
.IP \(rh
pen and paper
.LP
Whenever you think about the game,
or whenever you notice yourself getting stressed,
or every thirty minutes
(whichever happens first),
roll 2d6.
Read them left to right,
the first number being the tens digit
and the second number being the ones digit.
For example, if you rolled a 2 and a 3,
that's a 23.
Look up your number on the tables below
and do as instructed.
Repeat as many times as you like.
When finished,
write 1 - 3 sentences in your journal
about how you feel in this moment.
.IP TIP:
You can also just pick your favorite thing(s)
from the tables below and just do that.
.heading Tables
.LP
.\" 10 mind
\f[B]11:\f[] what is the most story-worthy thing that has happened to you today?
\f[B]12:\f[] repeat to yourself "I am enough" on each inhale, and "I have enough" on each exhale.
\f[B]13:\f[] what are you thankful for right now?
\f[B]14:\f[] what has delighted you today?
\f[B]15:\f[] visualize your favorite place.
\f[B]16:\f[] imagine happiness, health, and peace for yourself. for a friend. for a stranger.
.hr
.LP
.\" 20 body
\f[B]21:\f[] massage your temples and your scalp.
\f[B]22:\f[] touch your toes and dangle. sway side to side.
\f[B]23:\f[] trunk twists, march in place.
\f[B]24:\f[] take a short walk.
\f[B]25:\f[] roll out your neck clockwise and anticlockwise.
\f[B]26:\f[] massage your hands and roll out your wrist.
.hr
.LP
.\" 30 breath
\f[B]31:\f[] breathe in for 4, hold for 4, exhale for 4, hold for 4. Repeat 4 times.
\f[B]32:\f[] breathe on quarter of the way full and pause. halfway full, pause. three quarters, pause. Fill all the way up, pause. Exhale. Repeat.
\f[B]33:\f[] close your eyes and count to ten. repeat.
\f[B]34:\f[] place one hand on your belly and one hand on your chest. feel your stomach and ribs fill up and deflate as you breathe.
\f[B]35:\f[] close one nostril and breathe in. close that nostril and open the other and breathe out. breathe in again through that same nostril, then out the other. repeat.
\f[B]36:\f[] trace the fingers of one hand from thumb to pinkie. breathe in on the way up, breathe out on the way down.
.hr
.LP
.\" 40 balance
\f[B]41:\f[] stand on one foot for 8 breaths. repeat on the other side.
\f[B]42:\f[] drink some water.
\f[B]43:\f[] do tree pose.
\f[B]44:\f[] eat some fresh fruit.
\f[B]45:\f[] say hi to a friend.
\f[B]46:\f[] look at something far away for 8 breaths.
.hr
.LP
.\" 50 rest
\f[B]51:\f[] close your eyes.
\f[B]52:\f[] rub your hands together really fast to build heat. close your eyes and cup them with your hands. feel the warmth.
\f[B]53:\f[] go get a chocolate or tasty treat.
\f[B]54:\f[] enjoy a hot cup of nice tea.
\f[B]55:\f[] put some cold water on your face.
\f[B]56:\f[] squeeze all your muscles one by one then relax them.
.hr
.LP
.\" 60 place
\f[B]61:\f[] put one thing away.
\f[B]62:\f[] declutter your space.
\f[B]63:\f[] scan your body from toes to head.
\f[B]64:\f[] listen to a favorite song.
\f[B]65:\f[] go get some sunshine.
\f[B]66:\f[] go get some good smells.
.heading Mini Games
.IP \(rh
After rolling,
choose between your number written backwards or forwards,
or do both!
Example: if you roll a 2 and a 3,
choose between 23 and 32.
(or do both)
.IP \(rh
If both dice are a 1, 2, or a 3,
that is a MINOR ROLL. 
Soften or lessen something.
Roll again.
.IP
If both dice are a 4, 5, or a 6,
that is a MAJOR ROLL.
Increase or enhance something.
Roll again.
.IP \(rh
If you roll doubles,
that is a MIGHTY PORTENT.
Something great is happening.
Roll again.
.if n .pl \n[nl]u
.1C
.bp
