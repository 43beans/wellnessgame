.P1
.EH ''''
.OH ''''
.so src/wellness.tmac
.nr PO 1i
.PSPIC -C assets/wellness.eps 8.5i
.nr PO 1.5i
.nr PN 0
.sp 2v
\s-2Wellness is licensed CC BY by 43beans 2024.
Images used are in the public domain
with no attribution required.
Be excellent to each other.
May you be safe,
happy,
peaceful,
and well.\s0
.EH ''-%-''
.OH ''-%-''
.bp
.
.
.
.ds TITLE wellness
.ds CREATED 2024-03-11
.\" Document Info
.TL
\*[TITLE]
.AU
\*[AUTHOR]
.XS
preface
.XE
.
.
.
.\" Begin Document
.heading Preface
.LP
Welcome to
.I wellness !
This is a short compilation
of small games
about pausing to take care of yourself.
It was originally conceived of
by acdw\**
.FS
totally awesome
and a friend to animals.
Check him out at
.CW
https://www.acdw.net/
.FE
and developed by 43beans\**.
.FS
a hacker collective based on friendship:
.CW
https://codeberg.org/43beans/
https://43beans.itch.io/
.FE
.PP
You will find three small, one-page games in this volume:
.IP \(rh
.B "Wellness d66" :
A game that requires two six-sided dice
and provides 36 activities.
.IP \(rh
.B "Wellness d12" :
A game that requires one twelve-sided die
and suggests a few sample activities
based on the die roll.
.IP \(rh
.B "Wellness Lite" :
Requires two coins.
.LP
They are all essentially the same game,
differing only slightly in the effort required 
and materials needed to play them.
So you can choose which to play
based on your energy,
surroundings,
and other circumstances.
.PP
.I "Wellness d66" ,
for example,
requires almost no effort
because you don't have to think up any activities;
they are provided for you.
But you have to have two dice on you,
and the paper in order to look up your rolls.
.PP
.I "Wellness Lite" ,
on the other hand,
can be played anywhere,
with no additional materials
(besides two coins).
But you might have to think up some activities on your own.
.PP
Finally,
.I "Wellness d12"
occupies the middle ground.
You can commit its rules to memory
after playing a few times.
And after doing so
you only need a twelve-sided die to play.
.PP
It is my sincerest hope
that this game 
brings you peace
and comfort
and wellness.
.br
.sp 3v
.in 4i
dozens
.br
\*[CREATED]
.if n .pl \n[nl]u  \" if nroff, set page length to number of lines
.                  \" (prevent empty lines at end of text)

.bp
