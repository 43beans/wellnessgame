# WELLNESS GAME

be well

https://43beans.itch.io/wellness

## What You Will Find Here

```
.
├── README.md
├── assets
│   ├── dither_it_wellness.png
│   ├── wellness.eps
│   ├── wellness.png
│   ├── wellness.xcf
│   └── woman-570883_1280.jpg
├── dist
│   ├── wellness.pdf
│   └── wellness.txt
├── justfile
└── src
    ├── d12/
    ├── d66/
    ├── feed.rec
    ├── lite/
    ├── origedits/
    ├── original/
    ├── preface/
    ├── toc.txt
    └── wellness.tmac

10 directories, 38 files
```

Directories:

- `assets`: some images and thumbnails
- `dist`: pdf and txt compilation
- `src/d66`: A wellness game with 2 six-sided dice and a 36-item lookup table
- `src/d12`: Roll a twelve sided dice
- `src/lite`: flip two coins. smol enough to fit in your head.
- `src/origedits`: some light edits to the original Wellness RPG by Case Duckworth
- `src/original`: the original game by Case Duckworth

Files:

- `*.pdf`: a pdf rendering of the game
- `*.src.txt`: original sketches and rough drafts
- `*.t`: troff macros and markup
- `*.txt`: nroff output of the game
